/**************************************************************************/
/*                                                                        */
/*  This file is part of LReplay.                                         */
/*                                                                        */
/*  Copyright (C) 2007-2020                                               */
/*    CEA (Commissariat à l'énergie atomique et aux énergies              */
/*         alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file licenses/LGPLv2.1).            */
/*                                                                        */
/**************************************************************************/

static FILE* lreplay_file = (void*) 0;

struct labelHash *id_to_status;

//id_to_states stores all different bindings states for a given label
struct statesHash *id_to_states;

//id_to_data stores all informations related to a given label
struct dataHash *id_to_data;

// will be called at exit
// tmp value are used to allow to remove elements
// from hash while iterating on it
void lreplay_exit() {
  if (lreplay_file)  {
    struct labelHash *lStatus, *tmp_0;
    // Del & Free id_to_status hashtbl
    HASH_ITER(hh,id_to_status,lStatus,tmp_0){
      HASH_DEL(id_to_status,lStatus);
      free(lStatus);
    }

    struct statesHash *bStates, *tmp_1;
    // Del & Free id_to_states and id_to_data, and register all bindings states seen for each label
    HASH_ITER(hh,id_to_states,bStates,tmp_1){
      struct dataHash *bData;
      // get its data (should never return NULL in practice)
      HASH_FIND_INT(id_to_data,&bStates->key_idBind,bData);
      if(bData != NULL){
        binding_state *state,*tmp_2;
        // iter on all binding states of this label, and add a line per state
        // then remove it
        LL_FOREACH_SAFE(bStates->head, state, tmp_2){
          fprintf(lreplay_file, "%d,%d,%s,%s:%d,%d", bData->key_idBind, 1, bData->d->tag, bData->d->file, bData->d->line, bData->d->nbBinds);
          for (int i = 0; i < bData->d->nbBinds; i++) {
            fprintf(lreplay_file,",%s,%d", bData->d->vars[i], state->s[i]);
          }
          fprintf(lreplay_file,"%s\n", "");
          LL_DELETE(bStates->head, state);
          free(state);
        }
        HASH_DEL(id_to_data,bData);
        free(bData->d);
        free(bData);
      }
      HASH_DEL(id_to_states,bStates);
      free(bStates);
    }

    fclose(lreplay_file);
    lreplay_file  = (void*) 0;
  }
}

// will be called before main (GCC magic)
__attribute__((constructor))  void lreplay_init () {
  lreplay_file = fopen(xstr(LREPLAY_OUTPUT), "w");
  if (!lreplay_file) {
    return;
  }
  fprintf(lreplay_file, "# %s\n", xstr(LREPLAY_TESTDRIVER));
  fprintf(lreplay_file, "# id,condition value,file:line,tag\n");
  fflush(lreplay_file);

  id_to_status=NULL;
  id_to_states=NULL;
  id_to_data=NULL;

  atexit(lreplay_exit);
}

// Called for functions pc_label
void lbl_replay_report(const char* file,int line, int cond, int id, const char* tag) {

  if (!lreplay_file) return;

  cond = !!cond;

  // Goal: prevent a lot of superfluous coverage line that may occur when
  // label are in loops
  // -> the output file will contains at most (Number of labels)*2 lines
  struct labelHash *lblStatus;
  HASH_FIND_INT(id_to_status,&id,lblStatus);
  if (lblStatus == NULL) {
    lblStatus = malloc(sizeof(struct labelHash));
    lblStatus->key_idLabel=id;
    lblStatus->status = 1 + cond;
    HASH_ADD_INT(id_to_status,key_idLabel,lblStatus);
    fprintf(lreplay_file, "%u,%d,%s,%s:%u\n", id, cond, tag, file, line);
  }
  else{
    switch(lblStatus->status){
    case 1: // Reached negatively
      if (!cond) return; // nothing new here

      // Becomes covered!
      fprintf(lreplay_file, "%u,%d,%s,%s:%u\n", id, cond, tag, file, line);
      lblStatus->status = 3;
      break;

    case 2: // Reached positively (i.e. covered)
      if (cond) return; // nothing new here

      // Becomes reached negatively (are we interested in that?)
      fprintf(lreplay_file, "%u,%d,%s,%s:%u\n", id, cond, tag, file, line);
      lblStatus->status = 3;
      break;

    case 3: // Reached negatively and positively
      return;
    }
  }
  fflush(lreplay_file);

}

// Check if we already saw the state s in older states
int already_seen(binding_state *olders, int s[], int size){
  int is_same;
  struct binding_state *st;
  LL_FOREACH(olders, st){
    is_same = 1;
    for (int i = 0; i < size; i++){
      if (s[i] != st->s[i]) { is_same = 0; break;}
    }
    if (is_same) return 1;
  }
  return 0;
}


// Called for functions pc_label_bindings
void bi_replay_report(const char* file,int line, int cond, int id, const char* tag, int bindId, int nbBindings, ...) {

  if (nbBindings != 0) {
    cond = !!cond;
    if (!lreplay_file || !cond) return;
    int i;

    // Get variables and their values from va_list
    int s[nbBindings];
    char *vars[nbBindings];

    va_list ap;
    va_start(ap, nbBindings);
    for (i = 0; i < nbBindings; i++) {
      vars[i]=va_arg(ap, char*);
      s[i] = !!(va_arg(ap, int));
    }
    va_end(ap);

    // Create a new state with binding values
    struct binding_state *newState;
    newState = malloc(sizeof(struct binding_state) + sizeof(s));
    newState->next=NULL;
    for(i = 0; i < nbBindings; i++){
      newState->s[i]=s[i];
    }

    // Save data of this binding label for later
    struct dataHash *bData;
    HASH_FIND_INT(id_to_data,&id,bData);
    // Check if this label data are registred yet, register them if not
    if(bData == NULL){
      struct data *d;
      d = malloc(sizeof(struct data)+sizeof(vars));
      d->file=file;
      d->line=line;
      d->tag=tag;
      d->nbBinds=nbBindings;
      for(i = 0; i < nbBindings; i++){
        d->vars[i]=vars[i];
      }
      bData = malloc(sizeof(struct dataHash));
      bData->key_idBind=id;
      bData->d=d;
      HASH_ADD_INT(id_to_data,key_idBind,bData);
    }

    // If this is the first time we see this binding label, then create an entry
    // in our id_to_states hashtbl
    // else check if this state was already registered, and append it if not
    struct statesHash *bStates;
    HASH_FIND_INT(id_to_states,&id,bStates);
    if(bStates==NULL){
      bStates = malloc(sizeof(struct statesHash));
      bStates->key_idBind=id;
      bStates->head=newState;
      HASH_ADD_INT(id_to_states,key_idBind,bStates);
    }
    else {
      if (already_seen(bStates->head,s,nbBindings)) return;
      LL_APPEND(bStates->head,newState);
    }
  }
  else{
    lbl_replay_report(file,line, cond, id, tag);
  }

  return;
}
